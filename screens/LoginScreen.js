//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, Dimensions, Keyboard, StatusBar, AsyncStorage, ScrollView } from 'react-native';
// import { Images, Fonts, Colors } from './../Themes';
import { Icon, Container, Content } from 'native-base'
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

// create a component
class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      username: '',
      phoneNumber: '',
      hideShow: true,
      msgError: null,
      statusLoading: false
    }
    global.userInfo = {}

  }

  _handleSignup = () => {

  }
  _handleHideShow = () => {
    const { hideShow } = this.state
    this.setState({ hideShow: !hideShow })
  }
  _handleSignin = () => {

  }
  _handleEmail = (value) => {
    this.setState({ email: value })
  }

  _handleSignin = () => {
    const { username, email, phoneNumber, password } = this.state
    if (Actions.currentScene == 'Login') {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      var validEmail = email.replace(/\s/g, '');
      if (validEmail == '' || reg.test(validEmail) === false) {
        this.setState({ msgError: 'Your email is invalid' })
      } else {
        let userInfo = {
          username: username,
          email: email,
          phoneNumber: phoneNumber,
          password: password
        }
        Actions.Profile({ userInfo: userInfo })
        this.setState({msgError: false})
      }
    }
  }

  render() {
    const { email, password, hideShow, msgError, username, phoneNumber } = this.state
    return (
      <Container>
        <Content>
          <View style={styles.container}>
            <View style={styles.contentHeader}>
              <Image source={{ uri: 'https://i.pinimg.com/564x/bc/40/58/bc405854192629776271b47d5a9ff09b.jpg' }} style={{ width: 200, height: 200 }} />
            </View>
            <View style={styles.contentMiddle}>
              <View style={[styles.contentInput, { marginTop: '5%' }]}>
                <TextInput
                  style={styles.inputStyles}
                  placeholder='Username'
                  value={username}
                  onChangeText={(value) => this.setState({ username: value })}
                />
              </View>
              {msgError ?
                <Text style={{ fontSize: 14, color: 'red', marginLeft: 20, marginTop: 10 }}>* {msgError}</Text>
              : null}
              <View style={[styles.contentInput, { marginTop: '5%' }]}>
                <TextInput
                  style={styles.inputStyles}
                  placeholder='Email'
                  value={email}
                  onChangeText={(text) => this.setState({ email: text })}
                />
              </View>
              <View style={[styles.contentInput, { marginTop: '5%' }]}>
                <TextInput
                  style={styles.inputStyles}
                  placeholder='Phone Number'
                  keyboardType='numeric'
                  value={phoneNumber}
                  onChangeText={(text) => this.setState({ phoneNumber: text })}
                />
              </View>
              <View style={[styles.contentInput, { marginTop: '5%' }]}>
                <View style={{ width: '100%' }}>
                  <TextInput
                    style={styles.inputStyles}
                    placeholder='Password'
                    secureTextEntry={hideShow ? true : false}
                    value={password}
                    onChangeText={(text) => this.setState({ password: text })}
                  />
                  <TouchableOpacity onPress={this._handleHideShow} style={styles.btnEyeIcon}>
                    <Icon type="MaterialCommunityIcons" name={hideShow ? 'eye-off-outline' : 'eye-outline'} style={styles.hideShowIcon} />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={[styles.contentInput, { marginTop: '7%' }]}>
                <TouchableOpacity onPress={this._handleSignin} style={styles.btnStyles}>
                  <Text style={styles.btnText}>Sign In</Text>
                </TouchableOpacity>
              </View>
              <View style={[styles.contentForgot, { flexDirection: 'row', justifyContent: 'space-between', width: '90%', alignSelf: 'center' }]}>
                <View style={styles.rular} />
                <Text style={[styles.textForgot, { justifyContent: 'center', }]}>Connect with</Text>
                <View style={styles.rular} />
              </View>
            </View>
            <View style={[styles.contentFooter, { marginTop: msgError ? '15%' : '8%' }]}>
              <TouchableOpacity onPress={this._handleFacebookSignup} style={[styles.btnStyles, { flexDirection: 'row', justifyContent: 'center' }]}>
                <Icon type='EvilIcons' name='sc-facebook' style={styles.iconStyle} />
                <Text style={[styles.btnText, { marginLeft: 10 }]}>Facebook</Text>
              </TouchableOpacity>
              <View style={styles.contentSignup}>
                <Text style={{ color: '#b3b3b3', marginRight: 5 }}>Don't have account? </Text>
                <TouchableOpacity onPress={this._handleSignup} >
                  <Text style={[styles.btnText, { color: '#1C9CC6', fontWeight: '700' }]}> Create an account</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    justifyContent: 'space-between',
    height: height
  },
  contentHeader: {
    height: '20%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  contentMiddle: {
    height: '50%',
  },
  contentFooter: {
    padding: 20,
    height: '30%',
  },
  contentForgot: {
    marginTop: '7%',
    alignItems: 'center',
  },
  contentSignin: {
    width: '30%',
    alignItems: 'center',
    marginBottom: '10%'
  },
  text: {
    fontSize: 15,
    fontWeight: '700',
    color: '#000'
  },
  rular: {
    padding: 0.5,
    width: '37%',
    backgroundColor: '#ededed',
    marginTop: '1%'
  },
  contentInput: {
    alignSelf: 'center',
    width: '90%',
  },
  iconStyle: {
    color: '#fff',
    fontSize: 25,
    fontWeight: "bold"
  },
  contentIcon: {
    width: '20%',
    alignItems: 'center'
  },
  contentInputText: {
    width: '80%'
  },
  hideShowIcon: {
    color: '#b3b3b3',
    fontSize: 20,
  },
  btnEyeIcon: {
    position: 'absolute',
    top: '30%',
    left: '90%'
  },
  inputStyles: {
    fontSize: 14,
    width: '100%',
    borderRadius: 5,
    borderColor: '#b3b3b3',
    borderWidth: 0.5,
    borderRadius: 50,
    paddingLeft: 20
  },
  textForgot: {
    fontSize: 14 - 2,
    color: '#b3b3b3'
  },
  btnStyles: {
    padding: 13,
    backgroundColor: '#1C9CC6',
    borderRadius: 50
  },
  btnText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 14,
  },
  contentRow: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  contentSignup: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '7%'
  }
})
export default LoginScreen