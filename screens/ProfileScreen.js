import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, TextInput, Alert, AsyncStorage, } from 'react-native';
import { Container, Content, Icon, Picker } from 'native-base';
import { Actions } from 'react-native-router-flux';
class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: props.userInfo.email,
            phoneNumber: props.userInfo.phoneNumber,
            username: props.userInfo.username,
            password: props.userInfo.password,
            status: false,

        };
    }

    _handleSignOut = () => {
        if (Actions.currentScene == 'Profile') {
            Actions.reset('Login')
        }
    }

    _handleDone = () => {
        this.setState({ status: !this.state.status })
    }

    render() {
        const { email, phoneNumber, status, username, password } = this.state
        return (
            <Container>
                <Content>
                    <View style={styles.containerHeader}>
                        <View style={styles.contentProfile}>
                            <View style={{ width: '10%' }} />
                            <View style={styles.contentImageRound}>
                                <TouchableOpacity onPress={this._handleChangeProfile} disabled={status ? false : true} >
                                    <Image source={{ uri: "https://cdn.dribbble.com/users/86194/screenshots/8192965/media/b727fa75f9bfa7fd86c49afd286de581.png" }} style={styles.imageRound} />
                                    <View style={styles.contentIconRound}>
                                        <Icon name='camera' type='Entypo' style={styles.iconStyle} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity onPress={this._handleDone} style={styles.contentEditText}>
                                <Text style={styles.editText}>{status ? 'Done' : 'Edit'}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ padding: 10 }}>
                            <View style={{ alignSelf: 'center' }}>
                                <TextInput
                                    style={[styles.inputStyles, { fontSize: 20 }]}
                                    value={username}
                                    autoFocus={status ? true : false}
                                    onChangeText={(text) => this.setState({ username: text })}
                                    editable={status ? true : false}
                                />
                            </View>
                        </View>
                    </View>

                    <View style={styles.contentFields}>
                        <Text style={styles.rightSideStyles}>Email</Text>
                        <View style={styles.leftSideStyles}>
                            <TextInput
                                style={styles.inputStyles}
                                value={email}
                                onChangeText={(text) => this.setState({ email: text })}
                                editable={status ? true : false}
                            />
                        </View>
                    </View>
                    <View style={styles.contentFields}>
                        <Text style={styles.rightSideStyles}>Phone</Text>
                        <View style={styles.leftSideStyles}>
                            <TextInput
                                style={styles.inputStyles}
                                value={phoneNumber}
                                keyboardType='numeric'
                                onChangeText={(number) => this.setState({ phoneNumber: number })}
                                editable={status ? true : false}
                            />
                        </View>
                    </View>
                    <View style={styles.contentFields}>
                        <Text style={styles.rightSideStyles}>Password</Text>
                        <View style={styles.leftSideStyles}>
                            <TextInput
                                style={styles.inputStyles}
                                value={password}
                                secureTextEntry={true}
                                onChangeText={(number) => this.setState({ password: number })}
                                editable={false}
                            />
                        </View>
                    </View>
                    <TouchableOpacity onPress={this._handleSignOut} style={[styles.contentFields, { justifyContent: null, padding: 15 }]}>
                        <Icon name='logout' type='AntDesign' style={styles.iconLogoutStyle} />
                        <Text style={styles.signOutText}>Sign out</Text>
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    containerHeader: {
        flex: 1,
        alignItems: 'center',
        width: '100%',
        borderBottomColor: '#ededed',
        borderBottomWidth: 1,
        padding: 20
    },
    imageRound: {
        width: 100,
        height: 100,
        borderRadius: Platform.OS === 'ios' ? 50 : 70,
    },
    contentProfile: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    contentEditText: {
        width: '15%',
        marginTop: 10,
    },
    contentImageRound: {
        width: '80%',
        alignItems: 'center'
    },
    editText: {
        textAlign: 'center',
        fontWeight: '700',
        color: '#1C9CC6',
        fontSize: 15
    },
    contentIconRound: {
        width: 25,
        height: 25,
        borderRadius: 25,
        backgroundColor: '#ededed',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: '65%',
        right: '5%',
    },
    iconStyle: {
        fontSize: 15
    },
    textName: {
        fontSize: 15,
        fontWeight: '700'
    },
    contentCreditPoint: {
        flexDirection: 'row',
        justifyContent: 'center',
        width: '80%'
    },
    crediNpointStyle: {
        width: '50%',
        alignItems: 'center'
    },
    textStyles: {
        fontWeight: '700'
    },
    contentFields: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: '#ededed',
        borderBottomWidth: 1,
        alignItems: 'center'
    },
    rightSideStyles: {
        width: '40%',
        padding: 20,
        fontWeight: '700'
    },
    leftSideStyles: {
        width: '60%',
    },
    inputStyles: {
        fontSize: 15
    },
    iconLogoutStyle: {
        fontSize: 20,
        color: 'red',
        paddingLeft: 10
    },
    signOutText: {
        marginLeft: '5%',
        fontSize: 15,
        color: 'red'
    }
})
export default ProfileScreen

