import React, { Component } from 'react';
import { Router, Scene, Drawer, Stack } from 'react-native-router-flux';
import LoginScreen from './../screens/LoginScreen'
import ProfileScreen from './../screens/ProfileScreen'

class NavigationRouter extends Component {

 
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene title='Login' key="Login" component={LoginScreen} hideNavBar={true} />
          <Scene title='Profile' key="Profile" component={ProfileScreen} hideNavBar={false} />
        </Scene>
      </Router>
    );
  }
}

export default NavigationRouter
